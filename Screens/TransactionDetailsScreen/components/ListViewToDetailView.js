import React from "react";
import { StyleSheet, View, Text, Image, TouchableOpacity } from "react-native";
import infoIcon from "../../../assets/IconImages/info.png";

export const ListViewToDetailView = ({ description, data }) => {
  return (
    <View style={styles.container}>
      <Text>{description}</Text>
      <View style={styles.dataContainer}>
        <Text style={styles.detail}>{data}</Text>
        <TouchableOpacity>
          <Image source={{ uri: infoIcon }} style={styles.infoIcon} />
        </TouchableOpacity>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    borderBottomWidth: 2,
    borderBottomColor: "silver",
    paddingHorizontal: 25,
  },
  detail: {
    opacity: 0.5,
  },

  infoIcon: {
    width: 18,
    height: 18,
    margin: 4,
  },
  dataContainer: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
  },
});

import React from "react";
import { StyleSheet, Text, View, Image } from "react-native";
import { StatusBar } from "expo-status-bar";
import { Line } from "../TransactionsScreen/components/Line";
import { CustomBtn } from "../../components/CustomBtn";
import { ListViewToDetailView } from "./components/ListViewToDetailView";
import exportIcon from "../../assets/IconImages/download.png";

export const TransactionDetailsScreen = ({ route }) => {
  const { paymentDetail, type, payWith } = route.params.item;
  return (
    <View style={styles.container}>
      <StatusBar />
      <Text style={styles.transactionAmount}>7638 $</Text>
      <View style={styles.btnContainer}>
        <CustomBtn title="Card" />
        <CustomBtn title="Debt" />
      </View>

      <Text style={styles.heading}>Transaction Detail</Text>
      <Line />
      <ListViewToDetailView description="Payment Detail" data={paymentDetail} />
      <ListViewToDetailView description="Type" data={type} />
      <ListViewToDetailView description="Pay with" data={payWith} />

      <View style={styles.exportWrapper}>
        <Image source={{ uri: exportIcon }} style={styles.exportIcon} />
        <Text>Export</Text>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    paddingHorizontal: 20,
  },
  mainHeading: {
    fontSize: 25,
    fontWeight: "bold",
    marginTop: 30,
  },
  searchBox: {
    width: "100%",
    height: 40,
    borderWidth: 5,
    borderColor: "silver",
    textAlign: "center",
  },
  heading: {
    fontWeight: "bold",
  },

  btnContainer: {
    flexDirection: "row",
    alignSelf: "center",
  },
  transactionAmount: {
    fontSize: 30,
    alignSelf: "center",
    marginVertical: 15,
  },
  exportIcon: {
    width: 16,
    height: 16,
    marginHorizontal: 5,
  },

  exportWrapper: {
    flexDirection: "row",
    alignSelf: "flex-end",
    marginVertical: 30,
  },
});

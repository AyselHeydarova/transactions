import React from "react";
import { StyleSheet, Text, View, Image, TouchableOpacity } from "react-native";

export const TransactionListView = ({
  fullName,
  imageUrl,
  amount,
  currency,
  onPress,
}) => {
  return (
    <TouchableOpacity style={styles.container} onPress={onPress}>
      <View style={styles.customer}>
        <Image source={{ uri: imageUrl }} style={styles.image} />
        <Text style={styles.customerName}>{fullName}</Text>
      </View>

      <Text style={styles.amount}>
        {currency}
        {amount}
      </Text>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    marginVertical: 10,
  },
  image: {
    width: 50,
    height: 50,
    borderRadius: 25,
    marginRight: 20,
  },
  customer: {
    flexDirection: "row",
    alignItems: "center",
  },

  customerName: {
    fontWeight: "bold",
  },

  amount: {
    fontWeight: "bold",
    opacity: 0.4,
  },
});

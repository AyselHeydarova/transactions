import React from "react";
import { View, StyleSheet } from "react-native";

export const Line = () => {
  return <View style={styles.line} />;
};

const styles = StyleSheet.create({
  line: {
    width: "100%",
    height: 3,
    opacity: 0.5,
    backgroundColor: "silver",
    marginVertical: 15,
  },
});

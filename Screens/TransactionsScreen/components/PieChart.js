import React from "react";
import { StyleSheet, View, Text } from "react-native";

export const PieChart = () => {
  return (
    <View style={styles.pieChart}>
      <View style={styles.innerSide}>
        <Text style={styles.text}>25%</Text>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  pieChart: {
    width: 0,
    height: 0,
    borderTopWidth: 100,
    borderTopColor: "red",
    borderBottomWidth: 100,
    borderBottomColor: "silver",
    borderLeftWidth: 100,
    borderLeftColor: "silver",
    borderRightWidth: 100,
    borderRightColor: "silver",
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 100,
    transform: [{ rotate: "45deg" }],
  },

  innerSide: {
    width: 180,
    height: 180,
    borderRadius: 100,
    backgroundColor: "white",
  },

  text: {
    transform: [{ rotate: "-45deg" }],
    color: "red",
    fontSize: 25,
    position: "absolute",
    top: 70,
    left: 70,
  },
});

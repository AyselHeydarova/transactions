import React from "react";
import { StyleSheet, Text, View, TextInput, FlatList } from "react-native";
import { TransactionListView } from "./components/TransactionListView";
import { StatusBar } from "expo-status-bar";
import { Line } from "./components/Line";

import { TransactionDetailsData } from "../../Data/data";
import { PieChart } from "./components/PieChart";

export const TransactionsScreen = ({ navigation }) => {
  return (
    <View style={styles.container}>
      <StatusBar />
      <Text style={styles.mainHeading}>Transactions</Text>
      <TextInput style={styles.searchBox} placeholder="Search" />
      <Text style={styles.heading}>Performance</Text>
      <PieChart />
      <Line />
      <Text style={styles.heading}>Transactions</Text>
      <Line />

      <FlatList
        data={TransactionDetailsData}
        renderItem={({ item }) => (
          <TransactionListView
            fullName={item.fullName}
            imageUrl={item.imageUrl}
            amount={item.transactionAmount}
            currency={item.transactionCurrency}
            onPress={() =>
              navigation.navigate("TransactionDetails", {
                title: item.fullName,
                item: item,
              })
            }
          />
        )}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    paddingHorizontal: 20,
  },
  mainHeading: {
    fontSize: 25,
    fontWeight: "bold",
    marginTop: 30,
  },
  searchBox: {
    width: "100%",
    height: 40,
    borderWidth: 5,
    borderColor: "silver",
    textAlign: "center",
  },
  heading: {
    fontWeight: "bold",
  },
});

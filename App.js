import React from "react";
import { TransactionsScreen } from "./Screens/TransactionsScreen/TransactionsScreen";
import { RootNav } from "./navigation/RootNav";

export default function App() {
  return (
    <RootNav>
      <TransactionsScreen />
    </RootNav>
  );
}

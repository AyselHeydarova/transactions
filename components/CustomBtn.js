import React from "react";
import { TouchableOpacity, Text, StyleSheet } from "react-native";

export const CustomBtn = ({ title, onPress }) => {
  return (
    <TouchableOpacity onPress={onPress} style={styles.container}>
      <Text>{title}</Text>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  container: {
    width: 100,
    height: 30,
    borderRadius: 20,
    textAlign: "center",
    backgroundColor: "silver",
    justifyContent: "center",
    alignItems: "center",
    marginHorizontal: 5,
  },
});

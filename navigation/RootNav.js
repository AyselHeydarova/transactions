import React from "react";

import { NavigationContainer } from "@react-navigation/native";
import { TransactionStack } from "./TransactionStack";

export const RootNav = () => (
  <NavigationContainer>
    <TransactionStack />
  </NavigationContainer>
);

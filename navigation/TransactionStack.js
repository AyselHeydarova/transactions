import React from "react";
import { createStackNavigator } from "@react-navigation/stack";
import { TransactionsScreen } from "../Screens/TransactionsScreen/TransactionsScreen";
import { TransactionDetailsScreen } from "../Screens/TransactionDetailsScreen/TransactionDetailsScreen";

const { Navigator, Screen } = createStackNavigator();

export const TransactionStack = () => (
  <Navigator>
    <Screen
      name="Transactions"
      component={TransactionsScreen}
      options={{ headerShown: "false" }}
    />
    <Screen
      name="TransactionDetails"
      component={TransactionDetailsScreen}
      options={({ route }) => ({ title: route.params.title })}
    />
  </Navigator>
);

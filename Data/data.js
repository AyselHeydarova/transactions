export const TransactionDetailsData = [
  {
    id: "1",
    imageUrl:
      "https://images.unsplash.com/photo-1438761681033-6461ffad8d80?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=60",
    fullName: "Ada Lovelace",
    transactionAmount: 1300,
    transactionCurrency: "$",
    paymentDetail: "30 July 2020",
    type: "Debt",
    payWith: "Credit Account",
  },

  {
    id: "2",
    imageUrl:
      "https://images.unsplash.com/photo-1501196354995-cbb51c65aaea?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=60",
    fullName: "Mark Hopper",
    transactionAmount: 720.34,
    transactionCurrency: "$",
    paymentDetail: "26 July 2020",
    type: "Debt",
    payWith: "Credit Account",
  },

  {
    id: "3",
    imageUrl:
      "https://images.unsplash.com/photo-1488426862026-3ee34a7d66df?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=60",
    fullName: "Margaret Hamilton",
    transactionAmount: 587.45,
    transactionCurrency: "$",
    paymentDetail: "20 May 2020",
    type: "Debt",
    payWith: "Credit Account",
  },
];
